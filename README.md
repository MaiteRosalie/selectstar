
<div align="center">
<div id="top"></div>
 <h3 align="center">Select Start</h3>
</div>

## About The Project
At Select Star, we process a lot of data, give that data to our users and try to do it in the most
performant way possible. To simulate this process, we want you to build an application that
GETS data from an API and displays it in a table.


API: [Art Institute of Chicago API](https://api.artic.edu/docs/#introduction) 


### Requirements
- React / Typescript application
- Styled Components are a plus
- You can use any package in the React ecosystem

### Evaluation
The React app should build without errors (typically using npm or yarn). If there are
necessary steps required to get it to compile, those should be covered in `README.md`
- No crashes or bugs
- State management and performance
- Code is easily understood and communicative (eg. comments, variable names, etc).
- The UI/UX of the app since this is a Front-end challenge.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

<p align="right"><a href="#top">back to top</a></p> 
