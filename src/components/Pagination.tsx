import type { TPagination } from "../data/core/models";
import { styled } from "../theme";

const SDiv = styled("div", {
  textAlign: "center",
  display: "flex",
  marginTop: "$2",
  justifyContent: "center",
});

const SNumber = styled("button", {
  padding: "$1",
  border: "none",
  color: "white",
  background: "none",
  borderRadius: "0.1rem",
  "&:enabled": {
    cursor: "pointer",
    "&:hover": {
      background: "#00000038",
    },
  },
  variants: {
    active: {
      true: {
        fontWeight: "bold",
        background: "#ffffff29!important",
      },
    },
  },
});

export type TPaginationProps = {
  pagination: TPagination;
  onChange: (page: string) => void;
};

const getRange = (current: number, total: number, length: number) => {
  const min = 1;
  if (length > total) length = total;
  let start = current - Math.floor(length / 2);
  start = Math.max(start, min);
  start = Math.min(start, min + total - length);
  return Array.from({ length: length }, (el, i) => start + i);
};

export const Pagination = ({
  pagination: { current, total },
  onChange,
}: TPaginationProps) => {
  const range = getRange(current, total, 5);

  return (
    <SDiv>
      {range.map((n) => (
        <SNumber
          type="button"
          key={n}
          active={current === n}
          onClick={() => {
            onChange(String(n));
          }}
        >
          {n}
        </SNumber>
      ))}
    </SDiv>
  );
};
