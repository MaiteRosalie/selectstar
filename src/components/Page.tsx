
import { styled } from "../theme";

export const Page = styled("div", {
  width: '100%',
  height: '100%',
  maxWidth: '1400px',
  margin: '0 auto',
  padding: '$3',
});

