import type { FormEvent, ChangeEvent } from "react";
import { useRef } from "react";
import { styled } from "../theme";

const SForm = styled("form", {
  boxShadow: "rgba(0, 0, 0, 0.14) 0px 2px 10px",
  display: "flex",
  marginBottom: "$2",
  maxWidth: "30rem",
});
const SInput = styled("input", {
  borderRadius: "0.3rem 0 0 0.3rem",
  border: "none",
  padding: "$1",
  fontFamily: "inherit",
  flexGrow: 1,
  width: "100%",
});
const SButton = styled("button", {
  borderRadius: "0 0.3rem 0.3rem 0",
  padding: "$1",
  border: "none",
  "&:enabled": {
    cursor: "pointer",
  },
});

export type TSearchInputProps = {
  onSubmit: (value: string) => void;
};

export const SearchInput = ({ onSubmit }: TSearchInputProps) => {
  const ref = useRef({
    value: "",
  });

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    onSubmit(ref.current.value);
  };
  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    ref.current.value = e.target.value;
  };
  return (
    <SForm onSubmit={handleSubmit}>
      <SInput
        type="text"
        placeholder="Write something here..."
        onChange={handleChange}
      />
      <SButton>Search</SButton>
    </SForm>
  );
};
