import type * as Stitches from "@stitches/react";
import { styled, KFadeInRight } from "../theme";

const STable = styled("div", {
  boxShadow: "rgba(0, 0, 0, 0.14) 0px 2px 10px",
  borderRadius: "0.3rem",
  display: "flex",
  overflow: "hidden",
  height: "80vh",
  background: "white",
});

const SSCroll = styled("div", {
  width: "100%",
  display: "flex",
  overflow: "auto",
});

const SColumn = styled("div", {
  display: "flex",
  flexDirection: "column",
  color: "rgba(0, 0, 0, 0.58)",
  flexGrow: 1,
  flexShrink: 0,
  minWidth: "10rem",
});

const SCell = styled("div", {
  padding: "$2",
  borderBottom: "1px solid rgba(0, 0, 0, 0.07)",
  flexShrink: 0,
  display: "flex",
  span: {
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  variants: {
    color: {
      red: {
        background: "#f4d5d5",
      },
      green: {
        background: "#cdeac4",
      },
    },
  },
});

const SHeader = styled(SCell, {
  color: "hsl(249.7, 42.9%, 48%)",
});

const SRow = styled(SCell, {
  fontSize: "0.8rem",
  opacity: 0,
  animation: `${KFadeInRight} 200ms forwards`,
});

export type TTableProps = {
  columns: Array<{
    header: string;
    align?: string;
    rows: Array<{
      content: string;
      id: number;
      color?: Stitches.VariantProps<typeof SCell>["color"];
    }>;
    width: number;
  }>;
};

export const Table = ({ columns }: TTableProps) => {
  return (
    <STable>
      <SSCroll>
        {columns.map(({ header, rows, width, align = "left" }) => (
          <SColumn key={header} css={{ width: `${width}%` }}>
            <SHeader title={header} css={{ textAlign: align }}>
              <span>{header || "---"}</span>
            </SHeader>
            {rows.map(({ content, id, color }, i) => (
              <SRow
                key={id}
                title={content}
                color={color}
                css={{ textAlign: align, animationDelay: `${20 * i}ms` }}
              >
                <span>{content || "---"}</span>
              </SRow>
            ))}
          </SColumn>
        ))}
      </SSCroll>
    </STable>
  );
};
