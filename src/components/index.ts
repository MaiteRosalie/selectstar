export { Table } from "./Table";
export type { TTableProps } from "./Table";
export { Page } from "./Page";
export { SearchInput } from "./SearchInput";
export { Pagination } from "./Pagination";
