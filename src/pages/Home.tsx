import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import type { TSharedExhibitionsProps } from "../data/core/services";
import type { TExhibition } from "../data/core/models";
import type { TTableProps } from "../components";
import { useAppDispatch } from "../data/hooks";
import { SExhibitionsQuery } from "../data/selectors";
import { AFetchExhibitions, ASearchExhibitions } from "../data/actions";
import { Table, Page, SearchInput, Pagination } from "../components";

export const Home = () => {
  const [page, setPage] = useState("1");
  const [searchValue, setSearchValue] = useState("");
  const dispatch = useAppDispatch();
  const exhibitionsQuery = useSelector(SExhibitionsQuery);

  const getRows = (
    key: keyof TExhibition
  ): TTableProps["columns"][number]["rows"] =>
    exhibitionsQuery.data.map((item) => ({
      content: String(item[key]),
      id: item.id,
      color: item.isClosed ? "red" : "green",
    }));

  const columns = [
    { header: "Title", rows: getRows("title"), width: 15 },
    { header: "Description", rows: getRows("description"), width: 15 },
    {
      header: "Featured",
      rows: getRows("isFeatured"),
      width: 2,
      align: "center",
    },
    { header: "Gallery Title", rows: getRows("galleryTitle"), width: 10 },
    { header: "Type", rows: getRows("type"), width: 2 },
  ];

  useEffect(() => {
    const fetchFields: TSharedExhibitionsProps = {
      page,
      limit: "30",
      fields: [
        "id",
        "short_description",
        "is_featured",
        "gallery_title",
        "title",
        "type",
        "aic_end_at",
      ],
    };
    if (searchValue) {
      dispatch(
        ASearchExhibitions({
          q: searchValue,
          ...fetchFields,
        })
      );
    } else {
      dispatch(AFetchExhibitions(fetchFields));
    }
  }, [dispatch, page, searchValue]);

  return (
    <Page>
      <SearchInput onSubmit={setSearchValue} />
      <Table columns={columns} />
      <Pagination
        pagination={exhibitionsQuery.pagination}
        onChange={(v) => {
          setPage(v);
        }}
      />
    </Page>
  );
};
