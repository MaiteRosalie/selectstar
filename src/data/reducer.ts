import { createReducer } from "@reduxjs/toolkit";
import type { TExhibition, TPagination } from "./core/models";
import { AFetchExhibitions, ASearchExhibitions } from "./actions";

export type TState = {
  exhibitionsQuery: {
    data: TExhibition[];
    isLoading: boolean;
    pagination: TPagination;
  };
};

const initialState: TState = {
  exhibitionsQuery: {
    data: [],
    pagination: {
      offset: 0,
      total: 0,
      current: 0,
    },
    isLoading: false,
  },
};

export const rootReducer = createReducer(initialState, (builder) => {
  // AFetchExhibitions
  builder.addCase(AFetchExhibitions.pending, (state) => ({
    ...state,
    exhibitionsQuery: {
      data: [],
      pagination: state.exhibitionsQuery.pagination,
      isLoading: true,
    },
  }));
  builder.addCase(AFetchExhibitions.rejected, (state) => ({
    ...state,
    exhibitionsQuery: {
      data: [],
      pagination: state.exhibitionsQuery.pagination,
      isLoading: false,
    },
  }));
  builder.addCase(AFetchExhibitions.fulfilled, (state, action) => ({
    ...state,
    exhibitionsQuery: {
      data: action.payload.data,
      pagination: action.payload.pagination,
      isLoading: false,
    },
  }));
  // ASearchExhibitions
  builder.addCase(ASearchExhibitions.pending, (state) => ({
    ...state,
    exhibitionsQuery: {
      data: [],
      pagination: state.exhibitionsQuery.pagination,
      isLoading: true,
    },
  }));
  builder.addCase(ASearchExhibitions.rejected, (state) => ({
    ...state,
    exhibitionsQuery: {
      data: [],
      pagination: state.exhibitionsQuery.pagination,
      isLoading: false,
    },
  }));
  builder.addCase(ASearchExhibitions.fulfilled, (state, action) => ({
    ...state,
    exhibitionsQuery: {
      data: action.payload.data,
      pagination: action.payload.pagination,
      isLoading: false,
    },
  }));
});
