import type { TRootState } from "./store";

export const SExhibitionsQuery = (state: TRootState) => state.app.exhibitionsQuery;
