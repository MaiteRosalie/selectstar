import type { TExhibitionResponse, TSharedAPIResponse } from "./apiResponses";

type TGetExhibitionsResponse = {
  data: TExhibitionResponse[];
} & TSharedAPIResponse;

export type TSharedExhibitionsProps = {
  limit?: string;
  page?: string;
  fields: Array<keyof TExhibitionResponse>;
};
export type TGetExhibitionsProps = TSharedExhibitionsProps & {
  ids?: Array<string>;
};

const getExhibitions = async (
  props?: TGetExhibitionsProps
): Promise<TGetExhibitionsResponse> => {
  const params = JSON.stringify(props);

  return await fetch(
    `https://api.artic.edu/api/v1/exhibitions?params=${params}`
  ).then((response) => response.json());
};

export type TGetExhibitionsSearchProps = TSharedExhibitionsProps & {
  q?: string;
  sort?: string;
};

const getExhibitionsSearch = async (
  props?: TGetExhibitionsSearchProps
): Promise<TGetExhibitionsResponse> => {
  const params = JSON.stringify({
    query: {
      bool: {
        must: [{ match: { title: { query: props?.q } } }],
      },
    },
    limit: props?.limit,
    page: props?.page,
    fields: props?.fields,
  });

  return await fetch(
    `https://api.artic.edu/api/v1/exhibitions/search?params=${params}`
  ).then((response) => response.json());
};

export const services = {
  getExhibitions,
  getExhibitionsSearch,
};
