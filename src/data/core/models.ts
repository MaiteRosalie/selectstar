import type { TExhibitionResponse, TPaginationResponse } from "./apiResponses";

export type TExhibition = {
  id: number;
  model: string;
  description: string;
  title: string;
  galleryTitle: String;
  isFeatured: boolean;
  isClosed: boolean;
  type: string;
};

export const Exhibition = (
  res: Partial<TExhibitionResponse> = {}
): TExhibition => {
  const isPastDate = (date:string)=>  (new Date().getTime() - new Date(date).getTime()) < 0
  const mapped = {
    id: res?.id ?? 0,
    model: "exhibition",
    description: res?.short_description ?? "",
    title: res?.title ?? "",
    galleryTitle: res?.gallery_title ?? "",
    isFeatured: res?.is_featured ?? false,
    type: res?.type ?? '',
    isClosed: res?.aic_end_at ? isPastDate(res.aic_end_at):  false
  };

  return mapped;
};

export type TPagination = {
  offset: number;
  total: number;
  current: number;
};

export const Pagination = (
  res: Partial<TPaginationResponse> = {}
): TPagination => {
  const mapped = {
    offset: res?.offset ?? 0,
    total: res?.total_pages ?? 0,
    current: res?.current_page ?? 0,
  };

  return mapped;
};
