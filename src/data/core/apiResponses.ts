export type TPaginationResponse = {
  total: number;
  limit: number;
  offset: number;
  total_pages: number;
  current_page: number;
};

export type TExhibitionResponse = {
  id: number;
  api_model: string;
  api_link: string;
  title: string;
  type: string;
  is_featured: false;
  short_description: string;
  web_url: string;
  image_url: string;
  status: string;
  aic_start_at: string;
  aic_end_at: string;
  gallery_id: number;
  gallery_title: string;
  artwork_ids: number[];
  artwork_titles: string[];
  artist_ids: number[];
  site_ids: number[];
  image_id: number;
  alt_image_ids: number[];
  document_ids: number[];
  source_updated_at: string;
  updated_at: string;
  timestamp: string;
};
export type test = keyof TExhibitionResponse

export type TSharedAPIResponse = {
  pagination: TPaginationResponse;
  info: {
    license_text: string;
    license_links: string[];
    version: string;
  };
  config: {
    iiif_url: string;
    website_url: string;
  };
};

