import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import { rootReducer } from "./reducer";

export const rootStore = configureStore({
  reducer: {
    app: rootReducer,
  },
});

export type TAppDispatch = typeof rootStore.dispatch;
export type TRootState = ReturnType<typeof rootStore.getState>;
export type TAppThunk<TReturnType = void> = ThunkAction<
  TReturnType,
  TRootState,
  unknown,
  Action<string>
>;
