import { createAsyncThunk } from "@reduxjs/toolkit";
import type { TExhibition, TPagination } from "./core/models";
import { Exhibition, Pagination } from "./core/models";
import type {
  TGetExhibitionsProps,
  TGetExhibitionsSearchProps,
} from "./core/services";
import { services } from "./core/services";

export const AFetchExhibitions = createAsyncThunk<
  { data: TExhibition[]; pagination: TPagination },
  TGetExhibitionsProps | undefined
>("fetchExhibitions", async (params) => {
  const res = await services.getExhibitions(params);
  const data = res.data.map(Exhibition);
  const pagination = Pagination(res.pagination);
  return { data, pagination };
});

export const ASearchExhibitions = createAsyncThunk<
  { data: TExhibition[]; pagination: TPagination },
  any
>("searchExhibitions", async (params) => {
  const res = await services.getExhibitionsSearch(params);
  const data = res.data.map(Exhibition);
  const pagination = Pagination(res.pagination);
  return { data, pagination };
});
